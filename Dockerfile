# syntax=docker/dockerfile:1

# The defaults are for convenience to make this file work out of the box.
# They may not point to the latest version. Anyways, adjust them to fit your needs :)
ARG MB_DOCKER_VERSION="24"

FROM registry.gitlab.com/mostlybroken/updatechecker:latest AS updater

FROM docker:${MB_DOCKER_VERSION}-cli

RUN apk update \
  && apk upgrade \
  && apk add git curl skopeo jq yq-go coreutils \
  && rm -rf /var/cache/apk/*

COPY --from=updater /uc/v1/ /uc/v1/
